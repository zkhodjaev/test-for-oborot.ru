<?php

class db {
	private $db;
	private $sqliteerror;
	function open() {
		$this->db = sqlite_open("notesdb", 0666, $this->sqliteerror);
		if ($this->db == FALSE) {
			die($this->sqliteerror);
		}
		
		$this->create_tables();
	}
	
	function create_tables() {				
		$result = sqlite_query($this->db, "SELECT 1 FROM sqlite_master WHERE type='table' AND name='users';");
		
		$is_exist = (int)sqlite_fetch_single($result);
		
		if ($is_exist == 0) {
			sqlite_query($this->db , "CREATE TABLE users (
			 id integer PRIMARY KEY,
			 name text NOT NULL,
			 email text NOT NULL UNIQUE
			);");
			
			sqlite_query($this->db , "CREATE TABLE notes (
			 id integer PRIMARY KEY,
			 user_id integer,
			 title text NOT NULL,
			 content text NOT NULL
			);");
		}
	}
	
	function create_new_user($name, $email) {
		$name = sqlite_escape_string($name);
		$email = sqlite_escape_string($email);
		
		sqlite_query($this->db , "INSERT INTO users (
			 name, email
			) 
			VALUES (\"{$name}\", \"{$email}\");");
			
		$user_id = sqlite_last_insert_rowid($this->db);
		
		return $user_id;
	}
	
	function create_new_note($user_id, $title, $content) {
		$user_id = sqlite_escape_string($user_id);
		$title = sqlite_escape_string($title);
		$content = sqlite_escape_string($content);
		
		$result = sqlite_query($this->db , "INSERT INTO notes (
			 user_id, title, content
			) 
			VALUES (\"{$user_id}\", \"{$title}\", \"{$content}\");");
	}
	
	function get_notes($user_id) {
		$user_id = sqlite_escape_string($user_id);
		
		$result = sqlite_query($this->db , "SELECT id, title, content FROM notes WHERE user_id = {$user_id};");
		
		return sqlite_fetch_all($result);
	}
}